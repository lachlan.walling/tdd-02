const zipObject = (keys, values) =>
  keys.reduce((acc, key, index) => ({ ...acc, [key]: values[index] }), {})

const tokenizeRow = (row) =>
  row
    .split('|')
    .slice(1, -1)
    .map((value) => value.trim())

const parse = (tableString) => {
  if (tableString === '') return { header: [], rows: [] }
  const [headerString, ...bodyStrings] = tableString.split('\n')
  const header = tokenizeRow(headerString)
  const rows = bodyStrings
    .map((rowString) => rowString.trim())
    .filter((rowString) => rowString !== '')
    .map((rowString) => zipObject(header, tokenizeRow(rowString)))
  return {
    header,
    rows,
  }
}

module.exports = parse
