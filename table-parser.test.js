const parse = require('./table-parser.js')

describe('table-parser', () => {
  it('parses a table with no columns and no rows', () => {
    const table = ``
    expect(parse(table)).toEqual({
      header: [],
      rows: [],
    })
  })
  it('parses a table with 1 column and 0 rows', () => {
    const table = `| id |`
    expect(parse(table)).toEqual({
      header: ['id'],
      rows: [],
    })
  })
  it('parses a table with 1 column and 1 rows', () => {
    const table = `| id |
| 1 |`
    expect(parse(table)).toEqual({
      header: ['id'],
      rows: [{ id: '1' }],
    })
  })
  it('parses a table with 1 column and 2 rows', () => {
    const table = `| id |
| 1 |
| 2 |`
    expect(parse(table)).toEqual({
      header: ['id'],
      rows: [{ id: '1' }, { id: '2' }],
    })
  })
  it('parses a table with 3 columns and 0 rows', () => {
    const table = `| id | name | description |`
    expect(parse(table)).toEqual({
      header: ['id', 'name', 'description'],
      rows: [],
    })
  })
  it('parses a table with 3 columns and 2 rows', () => {
    const table = `| id | name | description |
| 1 | test1 | description1 |
| 2 | test2 | description2 |`
    expect(parse(table)).toEqual({
      header: ['id', 'name', 'description'],
      rows: [
        { id: '1', name: 'test1', description: 'description1' },
        { id: '2', name: 'test2', description: 'description2' },
      ],
    })
  })
  it('ignore leading white space', () => {
    const table = `\t  | id | name | description |
| 1 | test1 | description1 |
\t\t| 2 | test2 | description2 |`
    expect(parse(table)).toEqual({
      header: ['id', 'name', 'description'],
      rows: [
        { id: '1', name: 'test1', description: 'description1' },
        { id: '2', name: 'test2', description: 'description2' },
      ],
    })
  })
  it('ignore trailing white space', () => {
    const table = `| id | name | description |\t
| 1 | test1 | description1 |
| 2 | test2 | description2 |\t\t\t`
    expect(parse(table)).toEqual({
      header: ['id', 'name', 'description'],
      rows: [
        { id: '1', name: 'test1', description: 'description1' },
        { id: '2', name: 'test2', description: 'description2' },
      ],
    })
  })
  it('ignore empty rows', () => {
    const table = `| id | name | description |

| 1 | test1 | description1 |
| 2 | test2 | description2 |`
    expect(parse(table)).toEqual({
      header: ['id', 'name', 'description'],
      rows: [
        { id: '1', name: 'test1', description: 'description1' },
        { id: '2', name: 'test2', description: 'description2' },
      ],
    })
  })
})
